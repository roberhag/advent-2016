import re, sys
from numpy import *
s = open('inp10.txt','r').read()

output = zeros(21, dtype = int) #total value in given place
bot = zeros((210, 4), dtype = int) #lowto, highto, stored, stored
value = zeros(75, dtype = int) #Where is this chip now?
#positive means bot, negative means output

m1 = re.findall(r"bot ([0-9]+) gives low to ([a-z]+) ([0-9]+) and high to ([a-z]+) ([0-9]+)", s)
m2 = re.findall(r"value ([0-9]+) goes to bot ([0-9]+)", s)

def receive(nbot, val):
    if nbot >= 0: #giving to a bot
        givebot(nbot) #should now have at least one free
        if bot[nbot,-1]:  #put in -2 if -1 occupied
            bot[nbot, -2] = val
        else: #put in -1 first
            bot[nbot, -1] = val
        #print 'bot %i received %i' % (nbot, val)
        givebot(nbot) #give further if needed!
    else:
        output[-1 - nbot] += val #TODO is this index rigt?
        if (-1 - nbot) == 0:
            print 0, val
        if (-1 - nbot) == 1:
            print 1, val
        if (-1 - nbot) == 2:
            print 2, val
    value[val] = nbot

def givebot(i):
    if bot[i,-1] and bot[i,-2]: #this bot only gives if it has 2 things
        a = min((bot[i,-1], bot[i,-2]))
        b = max((bot[i,-1], bot[i,-2]))
        #if a == 17 and b == 61:
            #print i, '!!!'
        receive(bot[i,0], a)
        receive(bot[i,1], b)
        #print 'bot %i gave (%i, %i) to (%i, %i)' % (i, a, b, bot[i,0], bot[i,1])
        bot[i,-1] = 0
        bot[i,-2] = 0

print 'reading...'
i = 0
for m in m1:
    a = int(m[0])
    b = int(m[2])
    c = int(m[4])
    if m[1] == 'bot': #low
        bot[a,0] = b
    else: #output
        bot[a,0] = - b - 1 #negative indexes... lol
    if m[3] == 'bot': #high
        bot[a,1] = c
    else: #output
        bot[a,1] = - c - 1 #negative indexes... lol
    i += 1
    
for m in m2:
    a = int(m[0])
    b = int(m[1])
    receive(b, a)
    i += 1

#print 'starting choochoo'
for j in range(210):
    givebot(j)

print 'lines', i

for a in output[:3]:
    print a

print 'derp'

#for a in value[]:
#    print a
