from numpy import *

ooo = """14519,47295-54910,18357
45202,1108-9617,37834
34172,74888-38215,50481
4027,98283-83695,63613
41187,8287-94719,64497
49969,28072-21579,16713
96264,65077-17247,22643
41952,229-88333,84187
7527,68937-22294,31187
8800,49312-11296,83909
88405,32174-54748,58082"""

points = [(0,0), (99999,99999)]

for l in ooo.split():
    a,b = l.split('-')
    points.append(eval('(' + a + ')'))
    points.append(eval('(' + b + ')'))

pvs = zeros(len(points), dtype = int) + 200000

def color(p, v): #p should be larger than 0
    if v < pvs[p]:
        pvs[p] = v
        if p == 1: print v #Shows progress by printing current shortest route
        pp = p + 1 - 2*(p % 2) #
        pvs[pp] = v
        a = points[pp] #coordinate of target warp
        for i, b in enumerate(points):
            if i == p or i == pp: #no need to revisit endpoints of this wormhole
                continue
            l = abs(a[0] - b[0]) + abs(a[1] - b[1])
            color(i, v+l)

pvs[0] = 0
for i, b in enumerate(points):
    if i < 2: #already at start point, end point already colored
        continue
    l = b[0] + b[1]
    color(i, l)
    
