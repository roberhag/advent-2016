#This code plots the rectangles!
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
inf = open('knowinp22.txt')
inf = '''[
[
[0,0,4,1],
[7,0,8,2],
[6,2,8,3],
[5,1,6,3],
[4,0,5,1],
[6,0,7,2],
[4,2,5,3],
[2,1,4,3],
[0,1,2,2],
[0,2,2,3],
[4,1,5,2],
[5,0,6,1]
],
[
[0,0,4,1],
[7,0,8,2],
[6,2,8,3],
[5,1,6,3],
[4,0,5,1],
[6,0,7,2],
[4,2,5,3],
[2,1,4,3],
[0,1,2,2],
[0,2,2,3],
[4,1,5,2],
[5,0,6,1]
],
[
[1,1,3,3],
[3,1,4,2],
[1,3,2,4],
[2,3,3,4]
],
[
[0,0,4,1],
[7,0,8,2],
[5,1,6,4],
[6,0,7,2],
[4,0,5,1],
[4,2,5,3],
[2,1,4,3],
[0,2,2,3],
[0,1,2,2],
[6,2,8,3],
[5,0,6,1],
[4,1,5,2]
],
[
[0,0,4,1],
[0,0,4,1]
],
[
[0,0,4,1],
[7,0,8,2],
[5,1,6,3],
[6,0,7,2],
[2,1,4,3],
[0,2,2,3],
[0,1,2,2],
[6,2,8,3],
[5,0,6,1]
],
[
[0,0,1,1],
[1,0,2,1],
[1,0,3,1],
[3,0,4,1]
],
[
[0,0,4,1]
],
[
[0,0,1,1],
[0,1,1,2],
[0,2,1,3],
[0,3,1,4]
],
[
[0,0,4,1],
[7,0,8,2],
[6,2,8,3],
[5,1,6,3],
[6,0,7,2],
[4,2,5,3],
[2,1,4,3],
[0,1,2,2],
[0,2,2,3],
[4,1,5,2],
[5,0,6,1]
],
[
[0,0,4,1],
[7,0,8,2],
[5,1,6,3],
[6,0,7,2],
[4,0,5,1],
[4,2,5,3],
[2,1,4,3],
[-1,2,2,3],
[0,1,2,2],
[6,2,8,3],
[5,0,6,1],
[4,1,5,2]
],
[
[0,0,5,1],
[7,0,8,2],
[5,1,6,3],
[6,0,7,2],
[4,0,5,1],
[4,2,5,3],
[2,1,4,3],
[0,2,2,3],
[0,1,2,2],
[6,2,8,3],
[5,0,6,1],
[4,1,5,2]
],
[],
[
[0,0,1,1],
[1,0,2,1],
[2,0,3,1],
[3,0,4,1]
],
[
[0,0,4,1],
[7,0,8,3],
[5,1,6,3],
[6,0,7,2],
[4,0,5,1],
[4,2,5,3],
[2,1,4,3],
[0,2,2,3],
[0,1,2,2],
[6,2,8,3],
[5,0,6,1],
[4,1,5,2]
]
]'''
inf = inf.split('\n')
biggu = ''.join([line for line in inf])
u = eval(biggu)
print(len(u))

for sett in u:
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111, aspect='equal')
    for sq in sett:
        w = sq[2] - sq[0]
        h = sq[3] - sq[1]
        if w < 1 or h < 1:
            print(sq)
        ax1.add_patch(Rectangle((sq[0],sq[1]), w,h))
    plt.axis((-1,10,-1,10))
    plt.draw()
    plt.show()
    #input()

#Disse skal fkking stemme
#INSPECTED BY EYE MFDICKFUCKK
#true,true,false,false,true,false,true,true,true,false,false,true,XX,true,true #XX er ingen rektangler
#true, true, false, false, true, false, true, true, true, false, false, true, false, true, true
#true, true, false, false, false, false, false, true, true, false, false, false, false, true, false

#ok, da prøver vi mange komboer

#true, true, false, false, true, false, true, true, true, false, false, true, false, true, true
#true, true, false, false, true, true, true, true, true, false, false, true, false, true, true
#true, true, false, false, true, false, true, true, true, false, false, true, true, true, true
#true, true, false, false, true, true, true, true, true, false, false, true, true, true, true
