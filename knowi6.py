from numpy import *

jumps = 99
sqs = 1000
values = zeros((2*sqs + 1, 2*sqs + 1))

for i in xrange(-sqs, sqs+1):
    for j in xrange(-sqs, sqs+1):
        values[i,j] = i + j

coord = zeros(2)

#order of these is important
stencil = array([[-2,-1],[-2,1],[-1,-2],[-1,2],[1,-2],[1,2],[2,-1],[2,1]])
pval = zeros(8)

for k in xrange(jumps):
    l = 0
    pc = coord + stencil
    for c in pc:
        pval[l] = abs(values[c[0],c[1]] - values[coord[0],coord[1]])
        l += 1
    if values[coord[0],coord[1]] == 1000:
        values[coord[0],coord[1]] = 0
    else:
        values[coord[0],coord[1]] = 1000
    coord = pc[argmin(pval)]
    print(coord)
    print()

a = coord[1]
print (-2*a, a, 3*a)
a = 1000000000000000l - 1
print (-2*a, a, 3*a)
