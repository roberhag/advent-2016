#from numpy import *
import string

t = string.maketrans('10','01')
a = '01111010110010011'
wantlen = 35651584

while len(a) < wantlen:
    b = a[::-1]
    b = string.translate(b, t)
    a = '0'.join([a,b])

print 'chs'
def checksum(s):
    cs = []
    for i in xrange(0,len(s),2):
        cs.append('1' if s[i] == s[i+1] else '0')
    return ''.join(cs)

c = checksum(a[:wantlen])
while len(c) % 2 == 0:
    c = checksum(c)

print c
