from numpy import *
import hashlib, re, sys

salt = 'rrrbmfta'
#salt = 'ulqzkmiv'
possibles = [salt]
true = []
print('thinking...')
continuing = True
while continuing:
    #for i, path in enumerate(possibles):
    possibles.sort(key=len, reverse=True)
    path = possibles.pop()
    hasj = hashlib.md5(path).hexdigest()
    c0 = path.count('D') - path.count('U')
    c1 = path.count('R') - path.count('L')
    for j, d in enumerate(['U','D','L','R']):
        if int(hasj[j], base=16) > 10:
            nc = (c0 + int(j==3 - j==2), c1 + int(j==1 - j==0))
            if -1 in nc or 4 in nc:
                continue
            possibles.append(path + d)
            if nc == (3, 3):
                true.append(path[8:])
                print len(possibles)
                print true[-1]
                sys.exit()
