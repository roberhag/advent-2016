from numpy import *

def rom(ii):
    if ii == 0: return '0'
    if ii == 1: return 'I'
    if ii == 2: return 'II'
    if ii == 3: return 'III'
    if ii == 4: return 'IV'
    if ii == 5: return 'V'
    if ii == 6: return 'VI'
    if ii == 7: return 'VII'
    if ii == 8: return 'VIII'
    if ii == 9: return 'IX'
    if ii == 10: return 'X'
    if ii == 11: return 'XI'
    if ii == 12: return 'XII'
    if ii == 13: return 'XIII'
    return 'ZZZUZUZUZ'

cod = 'Your message was received with gratitude! We do not know about you, but Christmas is definitely our favourite holiday. The tree, the lights, all the presents to unwrap. Could there be anything more magical than that?! We wish you a happy holiday and a happy new year!'
pas = []
print('reading...')

nums = []
#cod = 'abcd! lol derp?'
bod = cod[::-1].lower()
print (ord('a'))


for c in bod:
    i = ord(c) - 96
    if i <1 or i > 26:
        continue
    j = int(i/2)
    nums.append(j)
    nums.insert(0, j+i%2)
    
dums = [rom(i) for i in nums]
chums = ', '.join(dums)

print(chums)

