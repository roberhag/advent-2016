from numpy import  *

tria = zeros([100,100], dtype = int)

for j, line in enumerate(open('kn21.txt').readlines()):
    a = line.split()
    i = len(a)
    tria[j,:i] = map(int, a)

trib = tria.swapaxes(0,1)
trib = rot90(rot90(trib))
tric = zeros([100,100], dtype = int)

for i in xrange(100):
    for j in xrange(i+1):
        tric[i,j] = tria[99 - i + j,j]

def biggu(tri):
    for i in xrange(99,0,-1):
        for j in xrange(i):
            tri[i-1,j] += max((tri[i,j], tri[i,j+1]))
    return tri[0,0]
trib = trib.copy()
print 'A', biggu(tria)
print 'B', biggu(trib)
print 'C', biggu(tric)
