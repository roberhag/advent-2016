#NB, This will run forever, try using advent22c instead ;)
from numpy import *
from itertools import permutations

ifile = open('inp24.txt')
ydim = 41
xdim = 179
mmaxx = ydim*xdim*2**7
checks = 8 #included start point
checkpoints = [str(i) for i in range(checks)]

pmaze=zeros([ydim,xdim], dtype=int32)

print('reading')
for y, line in enumerate(ifile):
    for x, c in enumerate(line.strip()):
        if c == '0': startcoord = (x,y)
        if c == '#': pmaze[y,x] = mmaxx
        if c in checkpoints:
            checkpoints[int(c)] = (x,y)
checkpoints[0] = startcoord

#print(checkpoints)
#for i in range(xdim):
#    print(''.join([str(int(pmaze[j,i]>0)) for j in range(ydim)]))

def finddist(c1, c2):
    x,y = c1
    visited = [(x,y)]
    #bin(n).count("1")
    maze = pmaze.copy()
    maze[y,x] = 1
    steps = 0
    while not maze[c2[1],c2[0]]:
        steps += 1
        if len(visited) == 0:
            print('UHUHU')
            break
        for tc in visited.copy():
            visited.remove(tc)
            x, y = tc
            neigh = ((x,y-1),(x+1,y),(x-1,y),(x,y+1))
            for xx, yy in neigh: 
                if not maze[yy,xx]: 
                    visited.append((xx,yy))
                    maze[yy,xx] = 1
    return steps

lens = zeros([checks, checks], dtype = int32) #8x8 matrix with distances
print('Pathfinding %i x %i matrix...' % (checks, checks))
for i, c1 in enumerate(checkpoints):
    for j, c2 in enumerate(checkpoints[:i]):
        lens[i,j] = finddist(c1, c2)
        lens[j,i] = lens[i,j]
#print(lens)
print('Finding shortest total path in %i permutations...' % (len(list(permutations(range(checks))))))
minsum = mmaxx
for perm in permutations(range(1,checks)):
    su = lens[0, perm[0]] + lens[0, perm[-1]]
    for i in range(checks-2):
        su += lens[perm[i], perm[i+1]]
        #if su>minsum: break
    minsum = min(su, minsum)
print('Shortest path is', minsum)
