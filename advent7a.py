import re, sys

def hasabba(s):
    m = re.search(r"([a-z])(?!\1)([a-z])\2\1", s)
    return m

lines = open('inp7.txt', 'r')
i = 0
for s in lines:
    b = re.findall(r"\[[a-z]+\]", s)
    a = re.split(r"\[[a-z]+\]", s)
    brk = False
    for bb in b:
        if hasabba(bb):
            brk = True
            break
    if brk: continue
    for aa in a:
        if hasabba(aa):
            i += 1
            break
print i
