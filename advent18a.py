
from numpy import *
from time import time
t0 = time()
s = '.^..^....^....^^.^^.^.^^.^.....^.^..^...^^^^^^.^^^^.^.^^^^^^^.^^^^^..^.^^^.^^..^.^^.^....^.^...^^.^.'
ss = '.' + s + '.' #padding with safe ghost zones!
width = len(ss) #two wider than truly
depth = 400000 #or 40

traps = zeros([depth, width], dtype = 'bool')

traps[0,:] = [0 if a=='.' else 1 for a in ss] #translate . and ^ to 0 and 1

for j in xrange(1,depth):
    source = traps[j-1]
    traps[j,1:-1] = logical_xor(source[:-2], source[2:])
    #Left and right tile are different -> this is a trap.

safes = depth*(width - 2) - sum(traps) #total squares minus traps

print safes, (time() - t0)
