import sys
from numpy import *
s = open('inp20.txt','r').readlines()
firewall = zeros([len(s),2], dtype = uint32)
for i, x in enumerate(s):
    firewall[i,0], firewall[i,1] = map(int, x.strip().split('-'))
firewall = firewall[firewall[:,0].argsort()]

if firewall[0,0] != 0:
    print 'Answer is 0!! It is not blocked lol'
    sys.exit()

block = long(firewall[0,1] + 1) #first unblocked until now
allowed = 0
for l, h in firewall:
    if l > block:
        allowed += l-block
        
    if h >= block:
        block = h + 1

print 4294967295 - block #Should print minus one if the last possible IP is blocked
print allowed
