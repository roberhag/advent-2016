import sys
from numpy import *
s = open('inp20.txt','r').readlines()
firewall = zeros([len(s),2], dtype = uint32)
for i, x in enumerate(s):
    firewall[i,0], firewall[i,1] = map(int, x.strip().split('-'))
firewall = firewall[firewall[:,0].argsort()]

if firewall[0,0] != 0:
    print 'Answer is 0!! It is not blocked lol'
    sys.exit()

block = firewall[0,1] + 1 #first unblocked until now
for l, h in firewall:
    if l > block:
        print block
        break
    if h >= block:
        block = h + 1
