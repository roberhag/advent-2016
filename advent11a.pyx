import numpy as np

nsav = 1000000
isav = 0
f = np.zeros(5, dtype = np.int16)
ffs = np.zeros((nsav, 5), dtype = np.int16)
ffsi = np.zeros(nsav, dtype = np.int32)


CG = 1
PG = 2
RG = 4
SG = 8
TG = 16

CM = 32
PM = 64
RM = 128
SM = 256
TM = 512

#f = [[],['SG', 'SM', 'PG', 'PM'],['TG','RG','RM','CG','CM'],['TM'],[]]
#f = [[],['SM', 'PG', 'PM'],['SG','TG','RG','RM','CG','CM'],['TM'],[]]
#f[1] = SM + PG + PM
#f[2] = SG + TG + RG + RM + CG + CM
#f[3] = TM

#f = [[],['CM'],['CG', 'PM'],['PG'],[]] #testcase
f[1] = CM
f[2] = CG + PM
f[3] = PG

besti = 20#70
found = False
totsies = [[],[2],[3,1],[4,2],[3]]

def isok(ii):
    hasM = False #unshielded microchip
    hasG = False #any generator
    gens = f[ii] %32
    if gens != 0: #this has a generator
        hasG = True
    if f[ii] > 31: #Has a microchip
        mics = f[ii]/32 #cuts away generators
        for i in [16, 8, 4, 2, 1]:
            if mics >= i:
                if gens >= i:
                    gens -= i; mics -= i
                else: #there is at least one microchip not attached to generator
                    hasM = True
                    break
    if hasM and hasG: return False
    return True

#@profile
def move(fro, to, a, b=None, i=0):
    global found, besti, ffs, ffsi, isav
    f[fro] -= a
    f[to] += a
    if b:
        f[fro] -= b
        f[to] += b
    alrdy = 0
    while alrdy < isav:
        if np.all(ffs[alrdy]==f):
            break
        alrdy += 1
        
    if alrdy < isav:
        indx = alrdy
    else: #this state is not found already
        ffs[isav] = f.copy()
        ffsi[isav] = (i + 1) #plz run the iteration if new state.
        indx = isav
        isav += 1
        
    if i < ffsi[indx]: #run the iteration if better version or new.
        ffsi[indx] = i
        if to==4 and (f[1] + f[2] + f[3]) == 0:
            #print 'We have a possib: ', i
            if i < besti:
                besti = i
                print besti
            #if i == besti: found = True #Use this line if want to print results
        elif isok(fro) and isok(to):
            tot = totsies[to]
            this = f[to]
            j = 512*2
            while j > 0:
                j /= 2
                if this < j:
                    continue
                this -= j
                for toto in tot:
                    k = j*2
                    thas = this
                    while k > 0:
                        k /= 2
                        if thas < k:
                            continue
                        thas -= k
                        bb = k
                        if j == k: bb = None
                        if i < besti: move(to,toto, j, bb, i+1)
                        if found: break
                    if found: break
                if found: break

    if found: print 'Move from %i to %i:' %(fro, to), a, 
    f[fro] += a
    f[to] -= a
    if b:
        f[fro] += b
        f[to] -= b
        if found: print b,
    if found: print
    f[fro].sort()

f[1].sort()
f[2].sort()
f[3].sort()
f[4].sort()
#move(2,1,SG)
move(2,1,PM) #testcase
#for ff in ffs:
#    print ff
print 'Read backwards!' ,besti
