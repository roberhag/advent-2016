#import scipy as sp
from gmpy2 import *
from numpy import binary_repr
from time import time
t0 = time()
s = '.^..^....^....^^.^^.^.^^.^.....^.^..^...^^^^^^.^^^^.^.^^^^^^^.^^^^^..^.^^^.^^..^.^^.^....^.^...^^.^.'
#s = '.^^.^.^^^^'
ss = '.' + s + '.' #padding with safe ghost zones!
width = len(ss) #two wider than truly
innerbit = width-1
depth = 400000# 400000 #or 40

btraps = mpz()
traps = [0 if a=='.' else 1 for a in ss] #translate . and ^ to 0 and 1
for i, c in enumerate(traps):
    btraps += c*2**i #convert to binary

tot = popcount(btraps) #total amount of traps

for j in xrange(1,depth):
    btraps = ((btraps/2) ^ (btraps*2)).bit_clear(0).bit_clear(innerbit)
    #print binary_repr(btraps, width), popcount(btraps)
    tot += popcount(btraps)
    #Left and right tile are different -> this is a trap.

safes = depth*(width - 2) - tot #total squares minus traps

print safes, (time() - t0)
