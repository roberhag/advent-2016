from numpy import *
pos = zeros(7, dtype = uint64)
leng = zeros(7, dtype = uint64)

pos[:] = [0, 5,8,1,7,1,0]
leng[:] = [10446744073709551615l,17,19,7,13,5,3]
pos += arange(7) #arrange holes accounting for droptime.
pos %= leng

#t = 0 #time is now index 0

#quick and dirty, perfect for part a, just few seconds on part b
while any(pos[1:]):
    pos += 1
    pos %= leng

'''

#smarter idea that should work better for part b, but alas I am dumb.
dt = 1
for i in range(1,7):
    print i,pos[0], dt
    while pos[0] >= 0:
        pos[0] -= dt
    while pos[i]:
        pos[0] += dt
        pos[i] += dt
        pos[i] %= leng[i]
    dt *= leng[i]   #Shiet, for dette trenger man primtallsfaktorisering...?
        #vent, tallene eeer primtall whattafakk hvorfor funker ikke dette daaa
'''

print pos[0]
