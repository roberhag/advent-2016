from numpy import *
siz = 52
siz2 = siz*siz
gru = zeros((siz, siz), dtype = int) + siz2
x, y = meshgrid(arange(siz), arange(siz))
vals = array(x*x + 3*x + 2*x*y + y + y*y + 1352, dtype = int)#1352
try:
    gru = load('grid%i.npy'%siz)
except:
    for j in xrange(siz):
        for i in xrange(siz):
            br = binary_repr(vals[j,i])
            k = 0
            for b in br:
                if b=='1':k+= 1
            if k%2 == 1: #odd, wall
                gru[j,i] = -1

    save('grid%i.npy'%siz, gru)
#print gru
k = 1

def color(j, i, k):
    global gru
    gru[j,i] = k
    if k == 51:return
    if i < siz-1:
        if gru[j,i+1] > k:
             color(j,i+1, k+1)
    if i > 0: 
        if gru[j,i-1] > k:
             color(j,i-1, k+1)
    
    if j < siz-1:
        if gru[j+1,i] > k:
             color(j+1,i, k+1)
    if j > 0: 
        if gru[j-1,i] > k:
             color(j-1,i, k+1)

#color(4,7,1) #test case

#color(39,31, 1) #part 1
#print gru
print gru[1,1] - 1

color(1,1, 1)

print sum(logical_and(gru>0,gru<60))
