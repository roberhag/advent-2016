import sys

# Sum of these two numbers gives state of a pair.
#tried and wrong: 28, 29, 39

#floor      1  2  3  4   (total 0 to 15)
#generator: 0  4  8 12
#chip:      0  1  2  3
sstate = [0, 0, 0, 0, 0, 5, 5, 6] #seven pairs, first is elevator
estate = [3, 15, 15, 15, 15, 15, 15, 15] #ending state
possibles = [sstate, 42]
visited = [sstate]
whenvi = [0]
#p = s[i] 
def possi(gs, cs):
    #print 'possi', cs, gs
    for c, g in zip(cs, gs):
        if (c != g): # pair s is not together
            for gg in gs:
                if gg == c: #there is at least one generator at this chips location
                    return False
    return True

def ssderp(s, e):
    print 'DERP', e, s
    sys.exit()

depth = 1
#while possibles:
while estate not in possibles:
    s = possibles.pop(0)
    if s == 42:
        print depth, len(visited)
        depth += 1 #all possibles now have depth 1 deeper
        possibles.append(42)
        s = possibles.pop(0)
        if s == 42:
            print 'No more moves!?'
            break
    e = s[0] #elevator
    gs, cs = zip(*[(p/4, p%4) for p in s[1:]]) #generators and chips
    #print depth, s, gs, cs
    m = 0 #the lowest floor with anything on. not worth moving anything below!
    for i in xrange(e):
        if i not in gs + cs:
            m += 1
        else:
            break
    pp = [] #new possible states
    for i in xrange(7): #gens
        if gs[i] != e: continue
        if e < 3:#move gen up
            gss = [gs[ii] + 1 if ii == i else gs[ii] for ii in xrange(7)]
            ss = [e+1] + sorted([gen*4 + chip for (gen, chip) in zip(gss, cs)])
            if (ss not in visited) and possi(gss, cs):
                pp.append(ss)
                visited.append(ss)
                #whenvi.append(depth)
                #print     'moving     gen       up ', ss, gss, cs
        if e > m: #move gen down
            gss = [gs[ii] - 1 if ii == i else gs[ii] for ii in xrange(7)]
            ss = [e-1] + sorted([gen*4 + chip for (gen, chip) in zip(gss, cs)])
            if (ss not in visited) and possi(gss, cs):
                pp.append(ss)
                visited.append(ss)
                #whenvi.append(depth)
                #print     'moving     gen      down', ss, gss, cs
        for j in xrange(i+1,7): #gen + gen
            if gs[j] != e: continue
            if e < 3:#move gens up
                gss = [gs[ii] + 1 if ii in [i, j] else gs[ii] for ii in xrange(7)]
                ss = [e+1] + sorted([gen*4 + chip for (gen, chip) in zip(gss, cs)])
                if (ss not in visited) and possi(gss, cs):
                    pp.append(ss)
                    visited.append(ss)
                    #whenvi.append(depth)
                    #print 'moving   gen + gen   up ', ss, gss, cs
            if e > m: #move gens down
                gss = [gs[ii] - 1 if ii in [i, j] else gs[ii] for ii in xrange(7)]
                ss = [e-1] + sorted([gen*4 + chip for (gen, chip) in zip(gss, cs)])
                if (ss not in visited) and possi(gss, cs):
                    pp.append(ss)
                    visited.append(ss)
                    #whenvi.append(depth)
                    #print 'moving   gen + gen  down', ss, gss, cs
        for j in xrange(7): #gen + chip
            if cs[j] != e: continue
            if e < 3: #move pair up
                gss = [gs[ii] + 1 if ii == i else gs[ii] for ii in xrange(7)]
                css = [cs[jj] + 1 if jj == j else cs[jj] for jj in xrange(7)]
                ss = [e+1] + sorted([gen*4 + chip for (gen, chip) in zip(gss, css)])
                if (ss not in visited) and possi(gss, css):
                    pp.append(ss)
                    visited.append(ss)
                    #whenvi.append(depth)
                    #print 'moving chip and gen  up ', ss, gss, css
            if e > m: #move pair down
                gss = [gs[ii] - 1 if ii == i else gs[ii] for ii in xrange(7)]
                css = [cs[jj] - 1 if jj == j else cs[jj] for jj in xrange(7)]
                ss = [e-1] + sorted([gen*4 + chip for (gen, chip) in zip(gss, css)])
                if (ss not in visited) and possi(gss, css):
                    pp.append(ss)
                    visited.append(ss)
                    #whenvi.append(depth)
                    #print 'moving chip and gen down', ss, gss, css
    for i in xrange(7): #chips
        if cs[i] != e: continue
        if e < 3:#move chip up
            css = [cs[ii] + 1 if ii == i else cs[ii] for ii in xrange(7)]
            ss = [e+1] + sorted([gen*4 + chip for (gen, chip) in zip(gs, css)])
            if (ss not in visited) and possi(gs, css):
                pp.append(ss)
                visited.append(ss)
                #whenvi.append(depth)
                #print     'moving     chip      up ', ss, gs, css
        if e > m: #move chip down
            css = [cs[ii] - 1 if ii == i else cs[ii] for ii in xrange(7)]
            ss = [e-1] + sorted([gen*4 + chip for (gen, chip) in zip(gs, css)])
            if (ss not in visited) and possi(gs, css):
                pp.append(ss)
                visited.append(ss)
                #whenvi.append(depth)
                #print     'moving     chip     down', ss, gs, css
        for j in xrange(i+1,7): #chips
            if cs[j] != e: continue
            if e < 3:#move chip up
                css = [cs[ii] + 1 if ii in (i,j) else cs[ii] for ii in xrange(7)]
                ss = [e+1] + sorted([gen*4 + chip for (gen, chip) in zip(gs, css)])
                if (ss not in visited) and possi(gs, css):
                    pp.append(ss)
                    visited.append(ss)
                    #whenvi.append(depth)
                    #print     'moving     chipS     up ', ss, gs, css
            if e > m: #move chip down
                css = [cs[ii] - 1 if ii in (i,j) else cs[ii] for ii in xrange(7)]
                ss = [e-1] + sorted([gen*4 + chip for (gen, chip) in zip(gs, css)])
                if (ss not in visited) and possi(gs, css):
                    pp.append(ss)
                    visited.append(ss)
                    #whenvi.append(depth)
                    #print     'moving     chipS    down', ss, gs, css
    possibles += pp
    #raw_input()
print 'ok', depth
    
for i, vv in enumerate(visited):
    e = vv[0]
    for ss in vv[1:]:
        if ss%4 != ss/4: #A chip is alone on floor ss%4
            if ss%4 in [x/4 for x in vv[1:]]: #list of floors with gens
                print 'STATE NOT ALLOWED?!', vv
                print ss%4, [x/4 for x in vv[1:]], [x%4 for x in vv[1:]]
                break
    #if vv == estate:
        #print whenvi[i]

