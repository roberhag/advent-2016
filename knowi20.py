from itertools import permutations
s = list(reversed(xrange(10)))

maxx = 0

for p in permutations(s):
    for i in xrange(1,10):
        a = int(''.join(map(str,p[:i])))
        b = int(''.join(map(str,p[i:])))
        if a*b > maxx:
            maxx = a*b
            print maxx
