#NB, This will run forever, try using advent22c instead ;)
from numpy import *
from random import randint

ifile = open('inp22.txt')
xnodes = 33
ynodes = 30
nnodes = xnodes*ynodes
import re
size, used, avail, proc = 0,1,2,3
try:
    nodes = load('nodes.npy')
    nodes2 = load('nodes2.npy')
    print('loaded')
except:
    nodes=zeros([xnodes,ynodes,4], dtype=int32)
    nodes2=zeros([nnodes,6], dtype=int32) #x, y, size, used, avail, %

    print('reading')
    i = 0
    for line in ifile:
        m = re.search(r'node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s+(\d+)\%', line)
        if not m:
            print('no nodes',line)
            continue
        x = tuple(map(int, m.group(1,2,3,4,5,6)))
        nodes[x[0],x[1]] = x[2:]
        nodes2[i] = x
        i += 1 
    save('nodes.npy', nodes)
    save('nodes2.npy', nodes2)
print('sorting')

nodes3 = nodes2.copy()
nodes2 = nodes2[nodes2[:,3].argsort()]
nodes3 = nodes3[(-nodes3[:,4]).argsort()]

#print('  Available       v')
#print(nodes3[0:10])
#print(nodes[:,:,1])
x, y = 15, 29 #hardcoded empty cell
gx, gy = 32, 0
gx, gy = 31, 0
gx, gy = 32, 1
c = 0 #count steps
mc = 0
bc = 999
p = 0 #index of possibilities
sstack = [(x,y, c, p)]
gstack = [(gx, gy)]
saved = [nodes.copy()]
visited = [(x,y)]
gvisited = [(gx, gy)]
vstack = [visited]

while len(sstack)>0:
    possi = []
    #print("(%2i,%2i): %i/%i" % (x,y,nodes[x,y,used],nodes[x,y,size]), end=' -> ')
    neigh = ((x,y-1),(x+1,y),(x-1,y),(x,y+1)) #"Heuristic" to reach gx and gy fast
    if (x,y) == (gx,gy):
        if c < bc:
            bc = c
            print('reached goal!', c)
    if c >= bc:
        neigh=()
    for xx, yy in neigh: 
        if xx<0 or xx==xnodes or yy<0 or yy==ynodes: continue
        #print("(%2i,%2i): %i/%i" % (xx,yy,nodes[xx,yy,used],nodes[xx,yy,size]), end=' or ')
        if nodes[xx,yy,used] > nodes[x,y,avail]: continue
        if (xx,yy) in visited: continue
        possi.append((xx,yy))
    if len(possi) <= p:
        nodes = saved.pop()
        x,y,c,p = sstack.pop()
        p += 1
        visited = vstack.pop()
        #print('Revert to', x, y, c, p)
        continue
    elif len(possi)>p+1:
        saved.append(nodes.copy())
        sstack.append((x,y,c,p))
        vstack.append(visited.copy())
    
    xx,yy = possi[p]
    nodes[x,y,avail] -= nodes[xx,yy,used]
    nodes[x,y,used] += nodes[xx,yy,used]
    nodes[xx,yy,used] = 0
    nodes[xx,yy,avail] = nodes[xx,yy,size]
    #print(xx,yy, len(possi))
    x,y = xx,yy
    visited.append((x,y))
    c += 1
    p = 0
print (visited)
print (mc, bc)
'''
while len(sstack)>0:
    possi = []
    #print("(%2i,%2i): %i/%i" % (x,y,nodes[x,y,used],nodes[x,y,size]), end=' -> ')
    if (gx,gy) == (32,0): #goal not moved yet
        neigh = ((x,y-1),(x+1,y),(x-1,y),(x,y+1)) #"Heuristic" to reach gx and gy fast
    elif (gx,gy) == (0,0):
        print('reached goal!', c+1)
        #break
    else:
        neigh = ((x-1,y),(x,y-1),(x,y+1),(x+1,y)) #reach 0 0 fast
        print("(%2i,%2i)  g (%2i,%2i)" % (x,y,gx,gy))
    for xx, yy in neigh: 
        if xx<0 or xx==xnodes or yy<0 or yy==ynodes: continue
        #print("(%2i,%2i): %i/%i" % (xx,yy,nodes[xx,yy,used],nodes[xx,yy,size]), end=' or ')
        if nodes[xx,yy,used] > nodes[x,y,avail]: continue
        if ((gx,gy)!=(32,0)):#Goal is moved
            if (xx,yy) == (gx,gy) and (x,y) in gvisited: continue
        if (xx,yy) in visited: continue
        possi.append((xx,yy))
    if len(possi) <= p:
        nodes = saved.pop()
        x,y,c,p = sstack.pop()
        p += 1
        gx, gy = gstack.pop()
        visited = vstack.pop()
        if((gx,gy)==(32,0)): #reset gvisited because we exhausted these possis.
            gvisited =[(gx, gy)]
        #print('Revert to', x, y, c, p)
        continue
    elif len(possi)>p+1:
        saved.append(nodes.copy())
        sstack.append((x,y,c,p))
        gstack.append((gx,gy))
        vstack.append(visited.copy())
    
    xx,yy = possi[p]
    nodes[x,y,avail] -= nodes[xx,yy,used]
    nodes[x,y,used] += nodes[xx,yy,used]
    nodes[xx,yy,used] = 0
    nodes[xx,yy,avail] = nodes[xx,yy,size]
    #print(xx,yy, len(possi))
    if (xx,yy) == (gx,gy):
        #print ('moving goal data (%2i,%2i) -> (%2i,%2i)' %(xx,yy,x,y))
        #break
        gx,gy = x,y
        gvisited.append((gx,gy))
        visited = []
    x,y = xx,yy
    visited.append((x,y))
    c += 1
    if c > mc:
        mc = c
    p = 0
print (visited)
print (mc)'''
