#Shortcut to find answer for part 1
ifile = open('inp22.txt')
xnodes = 33
ynodes = 30
nnodes = xnodes*ynodes
from numpy import *
import re
try:
    nodes = load('nodes.npy')
    nodes2 = load('nodes2.npy')
    print('loaded')
except:
    nodes=zeros([xnodes,ynodes,4], dtype=int32)
    nodes2=zeros([nnodes,6], dtype=int32) #x, y, size, used, avail, %

    print('reading')
    i = 0
    for line in ifile:
        m = re.search(r'node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s+(\d+)\%', line)
        if not m:
            print('no nodes',line)
            continue
        x = tuple(map(int, m.group(1,2,3,4,5,6)))
        nodes[x[0],x[1]] = x[2:]
        nodes2[i] = x
        i += 1 
    save('nodes.npy', nodes)
    save('nodes2.npy', nodes2)
print('sorting')
nodes3 = nodes2.copy()
nodes2 = nodes2[nodes2[:,3].argsort()]
nodes3 = nodes3[(-nodes3[:,4]).argsort()]
print('  Used      v')
print(nodes2[:5])
print('  Available    v')
print(nodes3[:5])
# The largest available amount...
print('Answer below assumes that the node above with most available is empty.')
print('.. And that no nodes will fit on the SECOND most available node.')
maxitaxi = nodes3[0,4]
i = 0
for node in nodes2: #I had only one node with enough free space, it had 94T
    if node[3] > 0 and node[3] <= maxitaxi: i += 1 


#print(nodes[:,:,1])
print(i)
