import string
def num2az(num):
    ndig = 0
    chars=[]
    uc=list(string.uppercase)
    uc.insert(0, uc.pop())
    while 26**ndig<num:
        d = (num%(26**(ndig+1)))/(26**ndig)
        chars.insert(0,uc[d])
        ndig += 1
    print ''.join(chars)

num2az(25)
num2az(26)
num2az(27)
num2az(53)
num2az(79)

num2az(90101894)
