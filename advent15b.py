from numpy import *
from time import time
pos = zeros(8, dtype = int)
leng = zeros(8, dtype = int)

pos[:] = [0, 5,8,1,7,1,0,0]
leng[:] = [9999999999,17,19,7,13,5,3,11]
pos += arange(8) #arrange holes accounting for droptime.
pos %= leng

#t = 0 #time is now index 0
tt = time()
#quick and dirty, perfect for part a, slow on part b
while any(pos[1:]):
    pos += 1
    pos %= leng

print 'time: %f s' % (time()-tt)
print pos[0]
