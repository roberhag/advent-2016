import re, sys
s = open('inp9.txt','r').read()



def recurse(s):
    i = 0
    dec = []
    m = re.search(r"\(([0-9]+)x([0-9]+)\)", s[i:])
    if not m:
        return len(s)
    while m:
        a = int(m.group(1))
        b = int(m.group(2))
        dec.append(m.start())
        dec.append(recurse(s[i+m.end():i+m.end()+a])*b)
        i += m.end()+a
        m = re.search(r"\(([0-9]+)x([0-9]+)\)", s[i:])
    dec.append(len(s[i:].strip()))
    s = sum(dec)
    return s
print recurse(s)
