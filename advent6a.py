from numpy import *
nums = zeros((8,26))
print('opening...')
inf = open('day6.txt','r')
print('reading...')
for line in inf:
    for i in range(8):
        nums[i,ord(line[i]) - 97] += 1
chars = argmin(nums, axis=1)
print(nums)
pas = [chr(97+i) for i in chars]
print(''.join(pas))
