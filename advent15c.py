from numpy import *
from time import time

pos = array([0, 5,8,1,7,1,0, 0], dtype = int)
leng = array([9999999999,17,19,7,13,5,3, 11], dtype = int)
pos += arange(len(pos)) #arrange holes accounting for droptime.
pos %= leng

t = 0

#super 1337 h4xx0r idea that should work better for part b.. !
tt = time()
dt = 1
for i in range(1,len(pos)):
    while pos[i]:
        t += dt #move time forward until previous wheels align again
        pos[i:] += dt #move all later wheels (previous are at 0 again)
        pos[i:] %= leng[i:]
    dt *= leng[i]   #time between wheels 1-i aligning.
t += dt

print 'time: %f s' % (time()-tt)
print t%dt #earliest time of alignment
