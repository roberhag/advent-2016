from numpy import *
import hashlib, re, sys

salt = 'rrrbmfta'
#salt = 'ulqzkmiv'
possibles = [salt]
largest = 0
print('thinking...')
while possibles:
    #for i, path in enumerate(possibles):
    possibles.sort(key=len, reverse=True)
    path = possibles.pop()
    if len(path) - 8 < largest: continue
    hasj = hashlib.md5(path).hexdigest()
    c0 = path.count('D') - path.count('U')
    c1 = path.count('R') - path.count('L')
    for j, d in enumerate(['U','D','L','R']):
        if int(hasj[j], base=16) > 10:
            nc = (c0 + int(j==3 - j==2), c1 + int(j==1 - j==0))
            if -1 in nc or 4 in nc:
                continue
            if nc == (3, 3):
                ii = len(path[8:])
                largest = max(largest, ii)
            else:
                possibles.append(path + d)

print largest
