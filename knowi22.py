#Creates the output for pasting into knowit... remove the last comma tho
import numpy as np
inf = open('knowinp22.txt')
biggu = ''.join([line for line in inf])
u = eval(biggu) #lag lister
painted = np.zeros([11,11],dtype=int)
for sett in u:
    sett = np.array(sett)
    sett += 1        #flytter alle tall opp ett hakk. Vil ikke ha -1.
    painted[:,:] = 0 #"piksler" i maleriet
    asum = 0
    if len(sett) ==1:
        print('true', end=', ')
        continue
    if len(sett) ==0:
        print('false', end=', ') #INGEN REKTANGEL I SETTET BETYR INGEN REKTANGEL
        #Bytt til 'true' her for ... det "riktige" svaret...
        continue
    minx, maxx = 12, 0
    miny, maxy = 12, 0
    for sq in sett:
        if sq[0] < minx: minx = sq[0]
        if sq[1] < miny: miny = sq[1]
        if sq[2] > maxx: maxx = sq[2]
        if sq[3] > maxy: maxy = sq[3]
        for x in range(sq[0],sq[2]): #traktor-metode som maler rektangel
            for y in range(sq[1],sq[3]):
                painted[x,y] += 1 
    bools = (painted >= 1) #Kan endre til == her for det "riktige" svaret hvor ingen rektangler overlapper
    print(str(bools[minx:maxx,miny:maxy].all()).lower(),end=', ')

