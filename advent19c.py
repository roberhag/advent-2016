from numpy import *

l = 3001330
gnomes = ones(l, dtype=uint)
oppos = arange(l, dtype = uint)
oppos += l/2
oppos %= l

cl = l
i = 0
while cl > 1:
    if gnomes[i]:           
        gnomes[i] += gnomes[oppos[i]]
        gnomes[oppos[i]] = 0
        if cl%2: #odd amount of gnomes
            oppos[i:oppos[i]] = oppos[i+1:oppos[i]+1]
        else: #even amount
            
            oppos[oppos[i]+1 :] = oppos[oppos[i] :]
        if hw < cl:
            i += 1
        cl -= 1
        if i >= cl:
            i = 0
    i = (i+1)%l
print (argmax(gnomes) + 1)
