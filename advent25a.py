ai = 0

inf = open('inp25.txt')
lines = inf.readlines() #list of instruction lines
lines.remove(lines[-1]) #in my code this makes it reloop infinite!

while ai < 170000: #I know it's smaller than this number...
    print (ai) #for tracking progress
    prevo = -1
    a = ai
    b = 0
    c = 0
    d = 0 #ai + 2550 
    ticks = 0
    tc = 0
    #able to save a state for each line.
    states = [(a,b,c,d,-1) for i in range(len(lines))] #a,b,c,d,togglecount.. one for each line
    i = 0
    while i < len(lines):
        line = lines[i]
        vals = line[4:].split()
        ins = line[:3]
        if ins == 'cpy':
            if vals[1] in ('a', 'b', 'c', 'd'):
                exec(vals[1] + '=' + vals[0])
        elif ins == 'inc':
            exec(vals[0] + '+= 1')
        elif ins == 'dec':
            exec(vals[0] + '-= 1')
        elif ins == 'out':
            #print(a,b,c,d)
            if prevo == -1:
                prevo = eval(vals[0]) #assuming out can only be 0 or 1.
            else:
                if eval(vals[0]) != (1-prevo) :  #Not in proper 01010101 loop
                    ticks = 0
                    break
                else: #We ARE in a proper 01010101 loop
                    ticks += 1 #counting the amount of ticks!
                    prevo = (1 - prevo) #tick.
        if ins == 'jnz' and eval(vals[0]):
            jmp = eval(vals[1])
            if jmp < 0 and i < 10: #Jumping backwards early, this can be used to inc dec a lot.
                aa, bb, cc, dd, ttc = states[i] #State last time this line was visited
                if tc == ttc:   #No toggles happened since last visit, 
                                #ASSUMING the same will happen again
                    x = vals[0] #The register value responsible for jump or not.
                    try:
                        reps = int(eval(x + '/(' + x + x + '-' + x + ')'))
                        a += (a-aa)*reps
                        b += (b-bb)*reps
                        c += (c-cc)*reps
                        d += (d-dd)*reps
                        jmp = 1 #Take shortcut and jump one ahead instead
                        states[i] = (a,b,c,d,-1) #next time here, save and jump again
                    except: #probably zero division, but any other problem is also OK
                        states[i] = (a,b,c,d,tc) #Just jump like normally instead
                                                 #This should be safe...
                else: #some toggles happened since last time! reset this state
                    states[i] = (a,b,c,d,tc)
            i += jmp
        else:
            i += 1
    if ticks > 2:
        print('THE ANSWER IS', ai)
        break
    ai += 1
#print (a, b, c, d)
#print ('a contains', a)

