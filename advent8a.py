from numpy import *

print('open')
lines = open('inp8.txt', 'r')
w = 50
h = 6
scr = zeros([2*w,2*h], dtype = uint) #With ghost zones

for s in lines:
    if s[:4] == 'rect':
        a, b = s[5:].split('x')
        a = int(a.strip())
        b = int(b.strip())
        scr[0:a,0:b] = 1
    elif s[:10] == 'rotate row':
        a, b = s[13:].split(' by ')
        a = int(a.strip())
        b = int(b.strip())
        b = b%w
        scr[b:b+w,a] = scr[0:w,a]
        scr[0:b,a] = scr[w:w+b,a]
        #print scr[:w,a]
    elif s[:10] == 'rotate col':
        a, b = s[16:].split(' by ')
        a = int(a.strip())
        b = int(b.strip())
        b = b%h
        scr[a,b:b+h] = scr[a,0:h]
        scr[a,0:b] = scr[a,h:h+b]
        #print scr[a,:h]

print sum(scr[:w,:h]) #part 1

for j in range(6):
    print ''.join([str(scr[k,j]) for k in range(50)]).replace('0', ' ').replace('1', '#')

