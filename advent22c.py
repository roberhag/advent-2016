#This is a GAME, which will probably only run on Windows and Python 3.5
#But install colorama and try on python too!
try:
    from msvcrt import getch
except:
    from getch import getch
from numpy import *
import os, sys
from time import time
try:
    from colorama import init, Fore, Back, Style
except:
    print('You will need colorama, dude')
    sys.exit(99)
init()
ifile = open('inp22.txt')
xnodes = 33
ynodes = 30
nnodes = xnodes*ynodes
import re
size, used, avail, proc = 0,1,2,3
try:
    nodes = load('nodes.npy')
    nodes2 = load('nodes2.npy')
    print('loaded')
except:
    nodes=zeros([xnodes,ynodes,4], dtype=int32)
    nodes2=zeros([nnodes,6], dtype=int32) #x, y, size, used, avail, %

    print('reading')
    i = 0
    for line in ifile:
        m = re.search(r'node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s+(\d+)\%', line)
        if not m:
            print('no nodes',line)
            continue
        x = tuple(map(int, m.group(1,2,3,4,5,6)))
        nodes[x[0],x[1]] = x[2:]
        nodes2[i] = x
        i += 1 
    save('nodes.npy', nodes)
    save('nodes2.npy', nodes2)
print('sorting')

nodes3 = nodes2.copy()
nodes2 = nodes2[nodes2[:,3].argsort()]
nodes3 = nodes3[(-nodes3[:,4]).argsort()]

def pprint(nodes, player, goal):
    li = ''
    t0 = time()
    for y in range(ynodes):
        for x in range(xnodes):
            us = nodes[x,y,used]
            si = nodes[x,y,size]
            stt = "%2i/%2i" %(us,si)
            if (x,y) == goal:
                co = Back.YELLOW + Style.BRIGHT
            elif (x,y) == player:
                co = Back.GREEN + Style.BRIGHT 
            elif (x,y) == (0,0):
                co = Back.BLUE + Style.BRIGHT  
            elif si>99:
                stt = "X%3iX" %(si)
                co = Back.RED
            else:
                co = Back.BLACK + Fore.WHITE + Style.DIM
            li += co+stt + Back.BLACK + ' '
        li += '\n'
    t1 = time()
    #print(li)
    sys.stdout.write(li)
    t2 = time()
    
    print('-'*10, (t1 - t0), (t2-t1), '-'*10)

#print('  Available       v')
#print(nodes3[0:10])
#print(nodes[:,:,1])
x, y = 15, 29 #hardcoded empty cell
gx, gy = 32, 0
c = 0 #count steps
mc = 0
bc = 999
p = 0 #index of possibilities
sstack = [(x,y, c, p)]
gstack = [(gx, gy)]
saved = [nodes.copy()]
visited = [(x,y)]
gvisited = [(gx, gy)]
vstack = [visited]
quit = True
print('Starting game loop... WASD move, Q quit, E undo.   Goal is to move yellow data to top left corner (blue). ')
print(
'||------------ Hey, did you remember to set screen width to at least 200 chars? 199 chars is this wide!              ... still going ...                                             ---------------||')
print('enter to start')
input()
while quit:
    possi = []
    #print("(%2i,%2i): %i/%i" % (x,y,nodes[x,y,used],nodes[x,y,size]), end=' -> ')
    pprint(nodes,(x,y),(gx,gy))
    print(c)
    if (gx,gy) == (0,0):
        print('reached goal with yellow data!', c)
    else:
        neigh = ((x-1,y),(x,y-1),(x,y+1),(x+1,y)) #reach 0 0 fast
        print("at:(%2i,%2i)  yellow data: (%2i,%2i)" % (x,y,gx,gy))
    for xx, yy in neigh: 
        if xx<0 or xx==xnodes or yy<0 or yy==ynodes: continue
        #print("(%2i,%2i): %i/%i" % (xx,yy,nodes[xx,yy,used],nodes[xx,yy,size]), end=' or ')
        if nodes[xx,yy,used] > nodes[x,y,avail]: continue
        possi.append((xx,yy))
    xixi = (-1,-1)
    while xixi not in possi:
        inp = ord(getch())
        if inp == 113: 
            quit = False
            break
        elif inp == 101: 
            possi = []
            break
        elif inp == 115: xixi = (x,y+1)
        elif inp == 97: xixi = (x-1,y)
        elif inp == 119: xixi = (x,y-1)
        elif inp == 100: xixi = (x+1,y)
        else:
            print('Did not recognize that input! Try again...')
    if len(possi) <= 0:
        nodes = saved.pop()
        x,y,c,p = sstack.pop()
        p += 1
        gx, gy = gstack.pop()
        visited = vstack.pop()
        if((gx,gy)==(32,0)): #reset gvisited because we exhausted these possis.
            gvisited =[(gx, gy)]
        print('Revert to', x, y, c, p)
        continue
    elif len(possi)>p+1:
        saved.append(nodes.copy())
        sstack.append((x,y,c,p))
        gstack.append((gx,gy))
        vstack.append(visited.copy())
    
    xx,yy = xixi
    nodes[x,y,avail] -= nodes[xx,yy,used]
    nodes[x,y,used] += nodes[xx,yy,used]
    nodes[xx,yy,used] = 0
    nodes[xx,yy,avail] = nodes[xx,yy,size]
    #print(xx,yy, len(possi))
    if (xx,yy) == (gx,gy):
        gx,gy = x,y
        gvisited.append((gx,gy))
        print('Moved goal!')
    x,y = xx,yy
    visited.append((x,y))
    c += 1
    if c > mc:
        mc = c
    p = 0
print (visited)
print (mc)
