dimn = [31,28,31,30,31,30,31,31,30,31,30,31]
diml = [31,29,31,30,31,30,31,31,30,31,30,31]
diyn = 365 
hh = 25
spd = 60*60*hh
y = 1970
m = 0
d = 0
h = 0
mi = 0
s = 0
ss = 0
sm = 2**31 - 1
while ss < sm:
    dim = dimn
    diy = diyn
    if y % 4 == 0: #Assuming no year 1900 or 2100!..
        dim = diml
        diy += 1
    m = m%12
    d = d%dim[m]
    h = h%hh
    mi = mi%60
    s = s%60
    dt = diy*spd
    y += 1
    if dt + ss > sm:
        y -= 1
        dt = dim[m]*spd
        m += 1
    if dt + ss > sm:
        m -= 1
        dt = spd
        d += 1
    if dt + ss > sm:
        d -= 1
        dt = 60*60
        h += 1
    if dt + ss > sm:
        h -= 1
        dt = 60
        mi += 1
    if dt + ss > sm:
        mi -= 1
        dt = 1
        s += 1
    ss += dt

print ("%4i-%02i-%02iT%02i:%02i:%02iZ" % (y, m + 1, d + 1, h, mi, s))
