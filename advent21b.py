import re

infile = open('inp21.txt', 'r')
infile = reversed(infile.readlines()) #backwards
st = list('fbgdceah')
#st=list('gbhcefad')
lenst = len(st)

def swap(a, b):
    st[a], st[b] = st[b], st[a]

def rotate(i):
    """Rotates LEFT by i chars"""
    global st
    st = st[i:] + st[:i]
print ''.join(st)
for op in infile:
    print op,
    d = re.findall(r'\d+',op) #all digits
    if op[:8] == "swap pos": #Swaps are their own inverse :)
        swap(int(d[0]), int(d[1]))
    elif op[:8] == "swap let":
        swap(st.index(op[12]), st.index(op[26]))
    elif op[:6] == "rotate":
        if op[7] == 'r': #now just rotate other direction
            rotate(int(d[0]))
        elif op[7] == 'l':
            rotate(lenst - int(d[0]))
        else: #based on index of X, HAS CHANGED!!
            x = op[35]
            i = st.index(x)
            j = [1,1,6,2,7,3,0,4][i]
            rotate(j)
    elif op[:7] == 'reverse':
        a,b = int(d[0]), int(d[1])
        st[a:b+1] = reversed(st[a:b+1])
    else: #Moving has changed a bit
        b,a = int(d[0]), int(d[1])
        x = st.pop(a)
        st.insert(b,x)
    print ''.join(st)
print ''.join(st)
